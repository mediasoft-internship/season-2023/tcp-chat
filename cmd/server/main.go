package main

import (
	"gitlab.com/mediasoft-internship/season-2023/tcp-chat/internal/server"

	"github.com/caarlos0/env"
	"log"
)

func main() {

	cfg := server.Config{}

	if err := env.Parse(&cfg); err != nil {
		log.Fatalf("failed to retrieve env variables, %v", err)
	}

	s := server.New()
	if err := s.Run(cfg); err != nil {
		log.Fatal(err)
	}
}
