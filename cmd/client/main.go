package main

import (
	"flag"
	"gitlab.com/mediasoft-internship/season-2023/tcp-chat/internal/client"
	"log"
)

var (
	nickname string
	addr     string
)

func init() {
	flag.StringVar(&nickname, "nickname", "user-xxxx", "client nickname")
	flag.StringVar(&addr, "addr", "0.0.0.0:13003", "server address")
}

func main() {
	flag.Parse()

	cfg := client.Config{
		Addr:     addr,
		Nickname: nickname,
	}

	if err := client.New(cfg).Run(); err != nil {
		log.Fatal(err)
	}
}
